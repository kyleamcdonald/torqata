import requests
import psycopg2
from typing import List
import json
from customer import Customer
from order import Order

def connect():
    conn = psycopg2.connect(dbname='postgres', host='127.0.0.1', port='5432', user='', password='')
    cur = conn.cursor()
    return cur, conn

def create_order_data():
    o = Order()
    orders = []

    for i in range(0,10000):
        orders.append(o.get_order())
    return orders

def create_customer_data(data):
    c = Customer()
    customers = []
    for i in range(0,500):
        name = c.generate_name()
        address = c.generate_address()
        state = c.generate_state(data)
        customers.append((name, address, c.city, state, c.zip_code ))
    return customers


def get_population_data() -> List:
    url = 'https://datausa.io/api/data?drilldowns=State&measures=Population&year=latest'
    res = requests.get(url)
    response = [(i['State'], i['Population']) for i in json.loads(res.text)['data']]

    return response

def send_population_to_db(cur, conn, data):
    cur.executemany("""INSERT into "adult_population_table" (state, population) values (%s, %s)""", data)
    conn.commit()
    conn.close()
    return

def send_customer_data(cur, conn, customers):
    cur.execute('Delete from "customer_table";')
    cur.executemany("""INSERT into "customer_table" (name, address, city, state, zip_code) values (%s, %s, %s, %s, %s)""", customers)
    conn.commit()
    # conn.close()
    return

def send_order_data(cur, conn, orders):
    cur.executemany("""INSERT into "order_table" (customer_id, type, qty, retail_price, order_date) values (%s, %s, %s, %s, %s)""", orders)
    conn.commit()
    conn.close()
    return


if __name__ == "__main__":
    cur , conn = connect()
    
    data = get_population_data()
    # Send population data to database
    send_population_to_db(cur, conn, data)

    customers = create_customer_data(data)
    send_customer_data(cur, conn, customers)

    orders = create_order_data()
    send_order_data(cur, conn, orders)

