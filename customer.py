import names
from random_word import RandomWords
import random
r = RandomWords()

class Customer:
    def __init__(self):
        self.city = 'Phoenix'
        self.zip_code = '85719'
        
        return

    def generate_name(self):
        return names.get_full_name()


    def generate_address(self):
        street_name = None
        while not street_name:
            street_name = r.get_random_word()
        suffix = ['ave.', 'place', 'st.', 'blvd.', 'court', 'way', 'drive', 'lane']
        street_number = random.randint(100,10000)
        self.address = street_name + ' ' + street_name
        num = random.randint(0,7)
        return str(street_number) + ' ' + street_name + ' ' + suffix[num]

    def generate_state(self, data):
        lucky_number = random.randint(0,50)
        self.state = data[lucky_number][0]
        return data[lucky_number][0]
    