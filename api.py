import logging

import flask
from flask import Response, request
from streams import send_order
import json
app = flask.Flask("torqata_pizza_order_api")
logger = flask.logging.create_logger(app)
logger.setLevel(logging.INFO)


@app.route("/health-check", methods=["GET"])
def health_check():
    """
    Endpoint to health check API
    """
    app.logger.info("Health Check!")
    return Response("All Good!", status=200)


@app.route("/order", methods=["POST"])
def get_org():
    """
    Endpoint to submit customer order
    :param order: dictionary 
    example payload:
    order = {
            order_id(int): 123,
            customer_id(int): 456,
            type(str): 'pepperoni',
            qty(int): 2,
            retail_price(float): 19.99,
            order_date(datetime(utc)): 2021-06-22T12:12:34
        }
    :return json: {'status_code': 200, 'response': {'text': 'Order Recieved'}}
    """
    response = {}
    try:
        order = request.form('order')  # Get the Org name 
        send_order(order)
    except Exception as e:
        return Response("Error", status=404)
    
    return Response(response=json.dumps(response, indent=2), status=200)
