import random 
import datetime

class Order:
    def __init__(self):
        self.customer_id = random.randint(0,10)

    def get_order(self):
        start_date = datetime.date(2020, 6, 24)
        end_date = datetime.date(2021, 6, 24)

        time_between_dates = end_date - start_date
        days_between_dates = time_between_dates.days
        random_number_of_days = random.randrange(days_between_dates)
        random_date = start_date + datetime.timedelta(days=random_number_of_days)

        types = [
            {
                'type': 'pepperoni',
                'price': 14.99
            },
            {
                'type': 'cheese',
                'price': 12.99
            },
            {
                'type': 'hawaiian',
                'price': 15.99
            },
            {
                'type': 'sausage',
                'price': 15.99
            },
            {
                'type': 'veggie',
                'price': 12.99
            },
            {
                'type': 'gluten-free',
                'price': 16.99
            }
        ]
        ran_num = random.randint(0,5)
        type = types[ran_num]['type']
        price = types[ran_num]['price']
        qty = random.randint(1,10)
        retail_price = f"{qty * price:.2f}"
        retail_price = float(retail_price)
        order_date = random_date
        customer_id = random.randint(1,500)

        return customer_id, type, qty, retail_price, random_date